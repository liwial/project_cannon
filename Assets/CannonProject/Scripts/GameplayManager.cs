﻿using UnityEngine;
using CannonProject.UI;

namespace CannonProject {

	public class GameplayManager : MonoBehaviour {

		[SerializeField] private int lostPoints = 5;

		private int points;

		private static GameplayManager instance;
		public static GameplayManager Instance {
			get {
				if (instance == null)
					instance = FindObjectOfType<GameplayManager> ();
				
				return instance;
			}
		}
			
		void Start () {
			points = 0;
		}

		public void RemovePoints() {
			this.points -= lostPoints;
			UIManager.Instance.PointsValue = this.points.ToString ();
		}

		public void AddPoints(int points) {
			this.points += points;
			UIManager.Instance.PointsValue = this.points.ToString ();
		}

	}

}