﻿using System.Collections.Generic;
using UnityEngine;

namespace CannonProject {

	public class BallEmitterManager : MonoBehaviour {

        [SerializeField] private Ball ballPrefab;
        [SerializeField] private float ballForce = 90f;
		[SerializeField] private int balls = 40;
		[SerializeField] private int deactivateBallTime = 5;
		[SerializeField] private Color endColor = Color.black;

        private Transform ballsHolder;

        private List<Ball> ballsList = new List<Ball>();

        void Start() {

            ballsHolder = GameObject.Find("BallsHolder").transform;

            for (int i = 0; i < balls; i++) {

                Ball ball = CreateBall();
                ball.gameObject.SetActive(false);

			}

		}
			
		void Update () {

			if (Input.GetMouseButtonDown (0)) {

				if (!ShootBasicBall ()) { // additional balls if needed

                    Ball ball = CreateBall();
                    Initializeball(ball);

				}
			}

		}

        private Ball CreateBall() {

            Ball ball = Instantiate(ballPrefab, transform.position, Quaternion.identity);
            ballsList.Add(ball);

            ball.transform.SetParent(ballsHolder);

            return ball;

        }

        private void Initializeball(Ball ball) {

            ball.transform.position = transform.position;
            ball.transform.rotation = transform.rotation;

            ball.GetComponent<Ball>().DeactivateballTime = deactivateBallTime;
            ball.GetComponent<Ball>().EndColor = endColor;
            ball.BallAddForce(transform.forward, ballForce);

        }

		private bool ShootBasicBall() {

			foreach (var ball in ballsList) {

				if (!ball.gameObject.activeSelf) {

					ball.gameObject.SetActive (true);

                    Initializeball(ball);

					return true;

				} 
			}

			return false;

		}

			
	}

}