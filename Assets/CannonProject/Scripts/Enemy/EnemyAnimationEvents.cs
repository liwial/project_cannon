﻿using UnityEngine;

namespace CannonProject.Enemy {

	public class EnemyAnimationEvents : MonoBehaviour {

		[SerializeField] private Animator animator;
		[SerializeField] private EnemyController enemyController;

		void Start() {

			animator = GetComponent<Animator> ();

		}

		public void BackToWalking() {

			animator.SetBool ("IsTakingDamage", false);

		}

		public void RemoveTheDead() {

			enemyController.RemoveTheDead ();

		}

	}

}
