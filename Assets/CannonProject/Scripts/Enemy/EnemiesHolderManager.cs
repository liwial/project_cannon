﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CannonProject.Enemy {

    [RequireComponent(typeof(EnemyParameters))]
	public class EnemiesHolderManager : MonoBehaviour {

		[SerializeField] private int enemies = 10;

		[SerializeField] private float positionZBack = 10f;
		[SerializeField] private float positionZFront = -19f;

		[SerializeField] private float positionXBackMin = -30f;
		[SerializeField] private float positionXBackMax = 30f;

		[SerializeField] private float positionXFrontMin = -7f;
		[SerializeField] private float positionXFrontMax = 7f;

		[SerializeField] private int spawnTime = 3;
		[SerializeField] private int growUpTime = 1;

		[SerializeField] private GameObject enemyPrefab;
		[SerializeField] private GameObject cannon;

		private EnemyParameters enemyParameters;

		private List<GameObject> enemiesList = new List<GameObject> ();

		private GameObject tmpEnemy;
		private int timeLeft;

		void Start () {

            enemyParameters = GetComponent<EnemyParameters>();
			timeLeft = spawnTime;

			for (int i = 0; i < enemies; i++) {

				Vector3 positionBack = new Vector3 ((float)Random.Range(positionXBackMin, positionXBackMax), 0, positionZBack);
				GameObject enemy = (GameObject)Instantiate (enemyPrefab, positionBack, Quaternion.identity);
				enemy.transform.Rotate (0, -180, 0);
				enemy.transform.SetParent (transform);

				int smallOrBig = Random.Range(0,100);

				if (smallOrBig < 50) { //SMALL enemy
					Vector3 positionFront = new Vector3 ((float)Random.Range (positionXFrontMin, positionXFrontMax), 0, positionZFront);

					EnemyData tmpEnemyData = new EnemyData (enemyParameters.SpeedSmall, enemyParameters.HitToDieSmall, positionBack, positionFront);
					enemy.GetComponent<EnemyController> ().EnemyData = tmpEnemyData;

					enemy.transform.localScale = new Vector3 (enemyParameters.ScaleSmall, enemyParameters.ScaleSmall, enemyParameters.ScaleSmall);
				} else { //BIG enemy
					Vector3 positionFront = cannon.transform.position;

					EnemyData tmpEnemyData = new EnemyData (enemyParameters.SpeedBig, enemyParameters.HitToDieBig, positionBack, positionFront);
					enemy.GetComponent<EnemyController> ().EnemyData = tmpEnemyData;

					enemy.transform.localScale = new Vector3 (enemyParameters.ScaleBig, enemyParameters.ScaleBig, enemyParameters.ScaleBig);
				}

				enemy.SetActive (false);

				enemiesList.Add (enemy);

			}

			tmpEnemy = new GameObject();

			StartCoroutine ("GrowUp");

			StartCoroutine ("LoseTime");
		}
		

		void Update () {

			if (timeLeft <= 0) {

				StartCoroutine ("GrowUp");

				timeLeft = spawnTime;
			}

		}

		private IEnumerator GrowUp() {

			float scale = 0;

			foreach (var enemy in enemiesList) {

				if (!enemy.activeSelf) {

					tmpEnemy = enemy;
					scale = enemy.transform.localScale.x;
					enemy.SetActive (true);
					break;

				}

			}

			float rate = 1f / growUpTime;
			float progress = 0;

			while (progress < 1.0) {

				float lerped = Mathf.Lerp (0, scale, progress);
				tmpEnemy.transform.localScale = new Vector3 (lerped, lerped, lerped);

				progress += rate * Time.deltaTime;

				yield return null;

			}

		}

		private IEnumerator LoseTime() {

			while (true) {

				yield return new WaitForSeconds (1);
				timeLeft--;

			}

		}

	}

}
