﻿using UnityEngine;

namespace CannonProject.Enemy {

	public class EnemyController : MonoBehaviour {

		private EnemyData enemyData;
		private Animator animator;
		private int dieIterator;
		private bool isDead;

		public EnemyData EnemyData {
			set { enemyData = value; }
		}
			
		void Start () {

			dieIterator = this.enemyData.hitToDie;

			animator = GetComponent<Animator> ();

			isDead = false;
		}

		void Update () {

			if (!isDead) {

				float step = this.enemyData.speed * Time.deltaTime;
				transform.position = Vector3.MoveTowards (transform.position, this.enemyData.targetPosition, step);

                Vector3 dir = this.enemyData.targetPosition - transform.position;
                float angle = AngleBeteenTwoPoints(dir);
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);

                if (transform.position.z <= this.enemyData.targetPosition.z) {
					this.gameObject.SetActive (false);
					transform.position = this.enemyData.startPosition;

					if (this.enemyData.hitToDie == 1)
						GameplayManager.Instance.RemovePoints ();
				}

			}

		}

        private float AngleBeteenTwoPoints(Vector3 direction) {

            return Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

        }

		private void OnTriggerEnter (Collider other) {

			if (other.tag == "Ball") {
				
				dieIterator--;

				if (dieIterator == 0) {
					GameplayManager.Instance.AddPoints (this.enemyData.hitToDie);
					animator.SetTrigger ("IsDead");
					isDead = true;

				} else {
					animator.SetBool ("IsTakingDamage", true);
				}

				other.gameObject.SetActive (false);
			}
			else if (other.tag == "Cannon") {

				if (this.enemyData.hitToDie == 3)
					GameplayManager.Instance.RemovePoints();
			}

		}

		public void RemoveTheDead() {

			this.gameObject.SetActive (false);
			transform.position = this.enemyData.startPosition;
			isDead = false;
			dieIterator = this.enemyData.hitToDie;

		}

	}

}