﻿using UnityEngine;

namespace CannonProject.Enemy {

	public class EnemyData {

		public float speed;
		public int hitToDie;

		public Vector3 startPosition;
		public Vector3 targetPosition;

		public EnemyData() {

		}

		public EnemyData(float speed, int hitToDie, Vector3 startPosition, Vector3 targetPosition) {
			this.speed = speed;
			this.hitToDie = hitToDie;
			this.startPosition = startPosition;
			this.targetPosition = targetPosition;
		}

	}

}
