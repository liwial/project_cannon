﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CannonProject.Enemy {

	public class EnemyParameters : MonoBehaviour {

		[SerializeField] private float speedSmallMin = 3.5f;
		[SerializeField] private float speedSmallMax = 4.5f;

		[SerializeField] private float speedBigMin = 2.5f;
		[SerializeField] private float speedBigMax = 3.0f;

		[SerializeField] private int hitToDieSmall = 1;
		[SerializeField] private int hitToDieBig = 3;

		[SerializeField] private float scaleSmall = 20f;
		[SerializeField] private float scaleBig = 40f;

		public float SpeedSmall {
			get { return Random.Range (speedSmallMin, speedSmallMax); }
		}

		public float SpeedBig {
			get { return Random.Range (speedBigMin, speedBigMax); }
		}

		public int HitToDieSmall {
			get { return hitToDieSmall; }
		}

		public int HitToDieBig {
			get { return hitToDieBig; }
		}

		public float ScaleSmall {
			get { return scaleSmall; }
		}

		public float ScaleBig {
			get { return scaleBig; }
		}

	}

}
