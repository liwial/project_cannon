﻿using UnityEngine;
using UnityEngine.UI;

namespace CannonProject.UI {

	public class UIManager : MonoBehaviour {

		[SerializeField] private Text pointsValue;  

		private static UIManager instance;
		public static UIManager Instance {
			get { 
				if (instance == null)
					instance = FindObjectOfType<UIManager> ();
				return instance;
			}
		}

		public string PointsValue {
			set { pointsValue.text = value; }
		}

	}

}