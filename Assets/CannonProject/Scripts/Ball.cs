﻿using System.Collections;
using UnityEngine;

namespace CannonProject {

	public class Ball : MonoBehaviour {

		private int deactivateBallTime;
        private Renderer ballRenderer;
        private Rigidbody ballRigidbody;
        private Color startColor;
		private Color endColor;

		public int DeactivateballTime {
			set { deactivateBallTime = value; }
		}

		public Color EndColor {
			set { endColor = value; }
		}

        void Awake() {

            ballRenderer = GetComponent<Renderer>();
            ballRigidbody = GetComponent<Rigidbody>();

        }

        void Start() {

            startColor = ballRenderer.material.color;
            
		}
			
		private void OnTriggerEnter (Collider other) {

			if (other.tag == "Plane") {
				StartCoroutine ("DeactivateBall");
			}

		}

		private IEnumerator DeactivateBall () {

			float scale = transform.localScale.x;

			float rate = 1f / deactivateBallTime;
			float progress = 0;

			while (progress < 1.0) {

				float lerpedScale = Mathf.Lerp (scale, 0, progress);
				transform.localScale = new Vector3 (lerpedScale, lerpedScale, lerpedScale);

                ballRenderer.material.color = Color.Lerp (startColor, endColor, progress);

				progress += rate * Time.deltaTime;

				yield return null;
			}

			gameObject.SetActive (false);
			transform.localScale = new Vector3 (scale, scale, scale);
            ballRenderer.material.color = startColor;

        }

        public void BallAddForce(Vector3 direction, float ballForce) {

            ballRigidbody.AddForce(direction * ballForce);

        }

	}

}