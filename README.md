# Project Cannon

Simple game in which player controls cannon with mouse and can shoot enemies. Big enemies die after 3 shootings, small after 1.

![alt text](Screenshots/cannon01.png "")
Gameplay

Enemies are animated with usage of Mecanim system. They have 3 states of animation: walking, taking damage and dying.

![alt text](Screenshots/cannon02.png "")
View from Unity editor